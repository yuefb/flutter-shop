
import 'package:flutter/material.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import '../../components/GoodShow.dart';

/*
 *  某个栏目下的商品列表
 */
class CategoryGoodPage extends StatefulWidget {
  String name;
  final int categoryId;
  CategoryGoodPage({Key key,this.name,this.categoryId}) : super(key: key);

  @override
  _CategoryGoodPageState createState() => _CategoryGoodPageState();
}

class _CategoryGoodPageState extends State<CategoryGoodPage> with SingleTickerProviderStateMixin {


  RefreshController _refreshController = RefreshController(initialRefresh: false);
  List<String> data = ["1", "2", "3", "4", "5", "6", "7", "8", "9"];
  var refresherKey = GlobalKey();

  Widget _showGoods() {
    return GridView(
      shrinkWrap:true,
      gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
        crossAxisCount: 2,
        mainAxisSpacing: 5,
        crossAxisSpacing: 5,
        childAspectRatio: 0.7,
      ),
      children: this.data.map((e) => GoodShowPage(title: e+"小米最新最强手机，宇宙第一r",goodUrl: "https://img12.360buyimg.com/babel/s320x320_jfs/t1/3262/37/12871/472669/5bd6ac33Ea3730aa2/e39037efaa0cc562.jpg!cc_320x320.webp",id: 1,)).toList(),
      padding: EdgeInsets.fromLTRB(5, 0, 5, 0),
    );
  }
  void _loadData() async {
    print("下拉触发事件");
    //monitor fetch data from network
    await Future.delayed(Duration(milliseconds: 1000));
    List list = this.data;
    for (int i = 10; i < 20; i++) {
      list.add("Item $i");
    }
    if (mounted) {
      setState(() {
        this.data = list;
      });
    }
    // _refreshController.loadFailed();
    _refreshController.loadComplete();
  }

  void _onRefresh() async {
    print("上拉触发事件");
    await Future.delayed(Duration(milliseconds: 1000));
    List<String> list = [];
    for (int i = 10; i < 15; i++) {
      list.add("Item $i");
    }
    if (mounted) {
      setState(() {
        this.data = list;
      });
    }
    _refreshController.refreshCompleted(resetFooterState:true);
    
  }

  
  Widget _getGoodList () {
    return   Container(child:
                SmartRefresher(
                                key: refresherKey,
                                controller: _refreshController,
                                enablePullUp: true,
                                enablePullDown: true,
                                child: _showGoods(),
                                physics: BouncingScrollPhysics(),
                                footer: ClassicFooter(
                                  loadStyle: LoadStyle.ShowWhenLoading,
                                  loadingText: "加载数据",
                                  canLoadingText: "释放刷新数据....",
                                  idleText: "刷新完成",
                                  failedText: "刷新失败",
                                  completeDuration: Duration(milliseconds: 1000),
                                ),
                                onLoading:_loadData,
                                onRefresh: _onRefresh,
                              ),
                );
  }

  List<Widget> _tabs = [];
  TabController _tabController;
  @override
  void initState() { 
    super.initState();
    for (int i=0; i< 10; i++){
      _tabs.add(Tab(text: "栏目 $i"));
    }
    _tabController = new TabController(length: _tabs.length, vsync: this);
    _tabController.addListener(() {
      var name = "栏目 ${_tabController.index}";
      List<String> list = [];
        for (int i = 10; i < 15; i++) {
          list.add("[$name] $i");
        }
        if (mounted) {
          setState(() {
            this.data = list;
          });
        }
      setState(() {
        refresherKey = GlobalKey();
        widget.name = name;
      });
      print(_tabController.index);//打印点击的索引
    });
  }

@override
  Widget build(BuildContext context) {
    return DefaultTabController(
    //导航栏的长度
      length: _tabs.length,
      child: Scaffold(
        appBar: AppBar(
          title: Text("${widget.name}"),
          // backgroundColor: Colors.red,
          centerTitle: true,
          bottom: TabBar(
            isScrollable: true, //可滚动
            indicatorColor: Colors.blueGrey, //指示器的颜色
            labelColor: Colors.blueGrey, //选中文字颜色
            unselectedLabelColor: Colors.white, //为选中文字颜色
            // indicatorSize: TabBarIndicatorSize.label, //指示器与文字等宽
            tabs: _tabs,
            controller: _tabController,
          ),
        ),
        body: TabBarView(
          physics: NeverScrollableScrollPhysics(), //禁止滑动
          children: this._tabs.map((e) => _getGoodList()).toList(),
        ),
      ),
    );
  }

  @override
  void dispose() {
    _refreshController.dispose();
    super.dispose();
  }
}