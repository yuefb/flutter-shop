import 'package:flutter/material.dart';
import '../home/Category.dart';
import '../home/Home.dart';
import '../home/User.dart';

class Toolbar extends StatefulWidget {

  Toolbar({Key key}) : super(key: key);


  @override
  _ToolbarState createState() => _ToolbarState();
}

class _ToolbarState extends State<Toolbar> {

  List _pages = [new HomePage(),new CategoryPage(),new UserPage()];

  int _currentIndex = 0;

  @override
  Widget build(BuildContext context) {
    var page = this._pages[_currentIndex];
    return Container(
      child: Scaffold(
        body: Container(
          // color: hex('#fff'),
          child: page,
        ),
        bottomNavigationBar: Theme(
          data: ThemeData(
            highlightColor: Colors.transparent,
            splashColor: Colors.transparent
          ),
          child: BottomNavigationBar(
            type: BottomNavigationBarType.fixed,
            items: [
              BottomNavigationBarItem(icon: Icon(Icons.home),label: "主页"),
              BottomNavigationBarItem(icon: Icon(Icons.category),label: "分类"),
              BottomNavigationBarItem(icon: Icon(Icons.person),label: "我的"),
            ],
            currentIndex: _currentIndex,
            onTap: (index) {
              setState(() {
                this._currentIndex = index;
              });
            },
          ),
        )
      ),
    );
  }
}
