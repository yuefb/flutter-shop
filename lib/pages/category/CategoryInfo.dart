import 'dart:math';

import 'package:flutter/material.dart';

import 'CategoryLeft.dart';
import 'CategoryRight.dart';

class CategoryInfoPage extends StatefulWidget {
  CategoryInfoPage({Key key}) : super(key: key);

  @override
  _CategoryInfoPageState createState() => _CategoryInfoPageState();
}

class _CategoryInfoPageState extends State<CategoryInfoPage> {
  
  int _selectIndex = 0;
  List<Widget> _categorys = [];

  @override
  void initState() {
    super.initState();
    this._setRightData(5);
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Row(
        children: [
          Expanded(
            child:Container(
              alignment: AlignmentDirectional.topStart,
              child: SingleChildScrollView(child: 
                Column(
                  children: _getLeftData(),
                ), 
              ),
            ), 
            flex: 2
          ),
          Expanded(child: Container(
                      alignment: AlignmentDirectional.topCenter,
                      child: SingleChildScrollView(
                            padding: EdgeInsets.all(5),
                            
                            child: Wrap(
                                    children:this._categorys,
                                    spacing:5,
                                    runSpacing: 25.0, // 纵轴（垂直）方向间距
                                ),
                          ),
                    ), 
                  flex: 5),
        ],
      ),
    );
  }

  void _setRightData(int count) {
    // CategoryRightPage
    List<Widget> list = [];
    for(int i=0;i<count;i++) {
      list.add(CategoryRightPage(i,"images/head.jpg","标题$i"));
    }
    setState(() {
      this._categorys = list;
    });
  }

  List<Widget> _getLeftData() {
    List<Widget> list = [];
    for(int i=0;i<30;i++) {
      list.add(CategoryLeftPage("主页 $i",Icons.home,i,selectIndex: (selectIndex) => _changeSelectIndex(selectIndex),getSelectIndex: () => _getSelectIndex(),));
      list.add(Divider(height: 0.1,));
    }
    return list;
  }

  _changeSelectIndex(value) {
    setState(() {
      this._selectIndex = value;
    });
    int count = Random().nextInt(30);
    print("生成的随机数：$count");
    this._setRightData(count);
  }

  _getSelectIndex(){
    return this._selectIndex;
  }
}
