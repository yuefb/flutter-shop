
import 'package:flutter/material.dart';

class CategoryLeftPage extends StatefulWidget {

  final String title;
  final IconData icon; 
  final int id;
  final selectIndex;
  final getSelectIndex;

  CategoryLeftPage(this.title,this.icon,this.id,{Key key,this.selectIndex,this.getSelectIndex}) : super(key: key);

  @override
  _CategoryLeftPageState createState() => _CategoryLeftPageState();
}

class _CategoryLeftPageState extends State<CategoryLeftPage> {

  bool flag = false;
  @override
  Widget build(BuildContext context) {
    return Container(
          color: widget.getSelectIndex() == widget.id ?Colors.red: Colors.white,
          padding: EdgeInsets.all(10),
          child:  InkWell(
                  child: Row(
                      children: [
                        Icon(widget.icon),
                        SizedBox(width:3),
                        Text(widget.title),
                      ],
                  ),
                  onTap: () {
                    setState(() {
                        widget.selectIndex(widget.id);
                    });
                    print("${widget.id} 被点击了");
                  }
                ),
    );
  }
}