
import 'package:flutter/material.dart';

class CategoryRightPage extends StatefulWidget {

  final String imagePaht;
  final String title;
  final int id;

  CategoryRightPage(this.id,this.imagePaht,this.title,{Key key}) : super(key: key);

  @override
  _CategoryRightPageState createState() => _CategoryRightPageState();
}

class _CategoryRightPageState extends State<CategoryRightPage> {
  @override
  Widget build(BuildContext context) {
    return InkWell(
      child: Container(
              height: 80,
              width: 80,
              child: Center(
                child:Column(children: [
                    ClipOval(
                      child: Image.asset("${widget.imagePaht}",height: 60,)
                    ),
                    Text("${widget.title}")
                  ],), 
              )
            ),
      onTap: () {
        print("你点中的分类为：${widget.id}");
        Map param = new Map();
        param["name"] = widget.title;
        param["categoryId"] = widget.id;
        Navigator.pushNamed(context, "/categoryGood",arguments:param );
      },
    );
  }
}