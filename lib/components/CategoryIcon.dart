import 'package:flutter/material.dart';

class CategoryIconPage extends StatefulWidget {
  final String name;
  final String imageUrl;
  final int id;

  CategoryIconPage({Key key, this.name, this.imageUrl,this.id}) : super(key: key);

  @override
  _CategoryIconPageState createState() => _CategoryIconPageState();
}

class _CategoryIconPageState extends State<CategoryIconPage> {
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      child: Column(
        children: <Widget>[
          Container(
              width: 30,
              height: 30,
              child: Image.asset(widget.imageUrl)
            ),
          SizedBox(
            height: 10,
          ),
          Text(widget.name),
        ],
      ),
      onTap: (){
        print("被点击了");
        Map param = new Map();
        param["name"] = widget.name;
        param["categoryId"] = widget.id;
        Navigator.pushNamed(context, "/categoryGood",arguments:param );
      },
    );
  }
}
