

import 'package:flutter/material.dart';

import '../pages/toolbar/Toolbar.dart';
import '../pages/good/GoodDetail.dart';
import '../pages/categoryGood/CategoryGood.dart';

class AppRouter {

  static final _routes = {
    '/': (BuildContext content, {Object args}) => Toolbar(),
    '/goodDetail': (BuildContext content, {int args}) => GoodDetailPage(id:args),
    '/categoryGood': (BuildContext content, {Map args}) => CategoryGoodPage(name: args["name"],categoryId: args["categoryId"],),
  };
  // 
  /// 监听route
  Route getRoutes(RouteSettings settings) {
    String routeName = settings.name;
    final Function builder = AppRouter._routes[routeName];
    print(settings);

    if (builder == null) {
      return null;
    } else {
      return MaterialPageRoute(
          settings: settings,
          builder: (BuildContext context) =>
              builder(context, args: settings.arguments));
    }
  }

}