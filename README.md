# Flutter

学习Flutter demo

## 简介

第一个运用Flutter实现的demo，后续会加入更多组件完善.

主要运用以下组件:

- 轮播图：       flutter_swiper
- 上拉下拉刷新：  pull_to_refresh
- toast提示框：  fluttertoast

## 截图展示
- ![主页](https://images.gitee.com/uploads/images/2021/0412/092233_35a0d213_854265.jpeg "1.jpg")
- ![分类](https://images.gitee.com/uploads/images/2021/0412/092251_45465747_854265.jpeg "2.jpg")
- ![个人中心](https://images.gitee.com/uploads/images/2021/0412/092317_0b9cda33_854265.jpeg "3.jpg")
- ![分类数据](https://images.gitee.com/uploads/images/2021/0412/092338_6a605b37_854265.jpeg "4.jpg")